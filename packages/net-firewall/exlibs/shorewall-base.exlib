# Copyright 2013-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ! [[ ${PN} == shorewall-core ]]; then
    require systemd-service
fi

export_exlib_phases src_configure src_compile src_install

DESCRIPTION="
Shorewall is an iptables-based firewall for Linux Systems. Its configuration is
very flexible, allowing it to be used in a wide range of firewall/gateway/router
and VPN environments.
"
HOMEPAGE="http://shorewall.net/index.html"
DOWNLOADS="mirror://shorewall/$(ever range 1-2)/${PN/6}-$(ever range 1-3)/${PNV}.tar.bz2"

REMOTE_IDS="freecode:${PN}"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

# There are no tests but the Makefiles always try to run the shorewall executable.
RESTRICT="test"

DEPENDENCIES="
    build+run:
        dev-lang/perl:=
    run:
        dev-perl/Digest-SHA1
        net-firewall/iptables
        sys-apps/iproute2
    suggestion:
        net-firewall/ipset
"

if ! [[ ${PN} == shorewall-core ]]; then
    DEPENDENCIES+="
        build+run:
            net-firewall/shorewall-core[~${PV}]
    "
fi

if [[ ${PN} == shorewall6 ]] || [[ ${PN} == shorewall6-lite ]]; then
    DEPENDENCIES+="
        run:
            dev-perl/Socket6
    "
fi

shorewall-base_src_configure() {
    local exherbo_config=(
        --annotated=
        --host=
        --initdir=
        --initfile=
        --initsource=
        --libexecdir=/usr/share
        --perllibdir=/usr/share/shorewall
        --sbindir=/usr/$(exhost --target)/bin
        --sharedir=/usr/share
        --sysconfdir=
        --sysconffile=
        --servicedir="${SYSTEMDSYSTEMUNITDIR}"
    )

    econf "${exherbo_config[@]}" VENDOR=linux
}

shorewall-base_src_compile() {
    :
}

shorewall-base_src_install() {
    DESTDIR="${IMAGE}" edo ./install.sh

    emagicdocs

    if [[ -f ${IMAGE}/${SYSTEMDSYSTEMUNITDIR}/${PN}.service ]]; then
        # Fix the config dir.
        edo sed -i -e "/EnvironmentFile=/s:sysconfig/${PN}:conf.d/${PN}.conf:" "${IMAGE}"/"${SYSTEMDSYSTEMUNITDIR}"/${PN}.service
    fi

    if ! [[ ${PN} == shorewall-core ]]; then
        keepdir /var/lib/${PN}
    fi
}

