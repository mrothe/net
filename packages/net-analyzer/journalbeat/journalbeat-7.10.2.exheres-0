# Copyright 2020-2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="Ships systemd journal entries to Elasticsearch or Logstash"
HOMEPAGE="https://www.elastic.co/products/${PN}"
DOWNLOADS="https://artifacts.elastic.co/downloads/beats/${PN}/${PNV}-linux-x86_64.tar.gz"

LICENCES="Elastic-License"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

RESTRICT="strip"

DEPENDENCIES=""

WORK="${WORKBASE}/${PNV}-linux-x86_64"

src_test() {
    :
}

src_install() {
    dobin ${PN}

    insinto /etc/${PN}
    doins *.yml

    insinto /usr/share/${PN}
    doins -r kibana

    keepdir /var/{lib,log}/${PN}

    install_systemd_files

    emagicdocs
}

pkg_postinst() {
    elog "To run the Index setup configure the Elasticsearch host under"
    elog "output.elasticsearch in /etc/journalbeat/journalbeat.yml and"
    elog "run:"
    elog "journalbeat setup \\"
    elog "    -c /etc/journalbeat/journalbeat.yml \\"
    elog "    --path.home /usr/share/journalbeat \\"
    elog "    --path.config /etc/journalbeat \\"
    elog "    --path.data /var/lib/journalbeat \\"
    elog "    --path.logs /var/log/journalbeat"
}

