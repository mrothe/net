# Copyright 2016 Julien Durillon <julien.durillon@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user="google" tag="v${PV}" ] cmake

SUMMARY="Memory Efficient Serialization Library"
HOMEPAGE+=" https://google.github.io/flatbuffers/"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-gcc10-fix.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    "-G" "Unix Makefiles"
    # flatc is only installed for the Release configuration
    --hates=CMAKE_BUILD_TYPE
    -DCMAKE_BUILD_TYPE:STRING='Release'
    # We need to set C/CXXFLAGS again because of the above
    -DCMAKE_C_FLAGS:STRING="${CFLAGS}"
    -DCMAKE_CXX_FLAGS:STRING="${CXXFLAGS}"
    -DFLATBUFFERS_BUILD_FLATC:BOOL=TRUE
    -DFLATBUFFERS_BUILD_SHAREDLIB:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DFLATBUFFERS_BUILD_TESTS:BOOL=TRUE -DFLATBUFFERS_BUILD_TESTS:BOOL=FALSE'
)

src_prepare() {
    cmake_src_prepare

    # remove -Werror
    edo sed \
        -e 's:-Werror ::g' \
        -i CMakeLists.txt
}

