# Copyright 2012-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_prepare

SUMMARY="submission tools for IRC notifications"
DESCRIPTION="
irkerd is a specialized IRC client that runs as a daemon, allowing other programs
to ship IRC notifications by sending JSON objects to a listening socket.
"
HOMEPAGE="http://www.catb.org/~esr/${PN}"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.gz"

REMOTE_IDS="freecode:${PN}"

UPSTREAM_RELEASE_NOTES="https://gitorious.org/${PN}/${PN}/blobs/master/NEWS [[ lang = en ]]"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-doc/asciidoc
        app-text/docbook-xml-dtd:4.1.2
        app-text/xmlto
        dev-util/pkg-config
"
# For proxy support, PySocks (https://pypi.python.org/pypi/PySocks) would be
# required. Since neither Exherbo nor me need it, I'm not adding it.

DEFAULT_SRC_INSTALL_EXTRA_DOCS=(
    filter-example.py
    filter-test.py
    install.txt
    irk
    irkerhook.py
)

irker_src_prepare() {
    default

    edo sed -i -e "s:\(pkg-config\):$(exhost --tool-prefix)\1:g" Makefile
    edo sed -i -e "s:/bin:/$(exhost --target)/bin:g" Makefile
}

